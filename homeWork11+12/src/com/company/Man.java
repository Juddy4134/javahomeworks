package com.company;

import java.text.ParseException;
import java.util.HashMap;
import com.company.Animals.Pet;

public final class Man extends Human {
    public Man() {
        super();
        gender = "Man";
    }
    public Man(String name, String surname, String  birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
        gender = "Man";
    }
    public Man(String name, String surname, String birthDate, int iq, HashMap<DayOfWeek, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
        gender = "Man";
    }
    @Override
    public void greetPets() {
        for(Pet p : getFamily().getPets()) System.out.printf("Здраствуй, %s\n", p.getNickname());
    }

    public void repairCar() {
        System.out.println("Что-то с ходовой, нужно заехать на СТО");
    }
}
