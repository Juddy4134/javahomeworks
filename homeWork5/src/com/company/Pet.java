package com.company;

import java.util.Arrays;

public class Pet {
    private String nickname;
    private Species species;
    private int age, trickLevel;
    private String[] habits;
    private final ConstructorTypes currentType;
    private boolean isEmpty = false;

    private enum ConstructorTypes {
        SMALL("%s{nickname='%s', %s}"),
        MAX("%s{nickname='%s', %s, age=%d, trickLevel=%d, habits=%s}"),
        EMPTY("pet{}");

        String toStringText;
        ConstructorTypes(String toStringText) {
            this.toStringText = toStringText;
        }
    }

    public void setNickname(String nickname) {this.nickname = nickname;}
    public String getNickname() {return nickname;}
    public void setSpecies(Species species) {this.species = species;}
    public Species getSpecies() {return species;}
    public void setAge(int age) {this.age = age;}
    public int getAge() {return age;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public int getTrickLevel() {return trickLevel;}
    public void setHabits(String[] habits) {this.habits = habits;}
    public String[] getHabits() {return habits;}

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.currentType = ConstructorTypes.SMALL;
    }

    public Pet(Species species, String nickname, int age,int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = Arrays.copyOf(habits, habits.length);

        this.currentType = ConstructorTypes.MAX;
    }

    public Pet() {
        this.currentType = ConstructorTypes.EMPTY;
        isEmpty = true;
    }

    public static void eat() {
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.printf("\nПривет, хозяин. Я - %s. Я соскучился!", nickname);
    }
    public static void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        String petAbilities = String.format("canFly=%s, numberOfLegs=%s, hasFur=%s", Boolean.toString(species.canFly), Integer.toString(species.numberOfLegs), Boolean.toString(species.hasFur));
        return String.format(currentType.toStringText, species.getValue(), nickname, petAbilities, age, trickLevel, Arrays.toString(habits));
    }

    @Override
    public boolean equals(Object pet){
        if (pet == this){
            return true;
        }
        Pet cutie = (Pet) pet;
        if (this.getNickname().equals(cutie.getNickname()) && this.getAge() == cutie.getAge()){
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int hashCode() {
        if (this.isEmpty){
            return 1;
        }
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + (nickname == null ? 0 : nickname.hashCode());
        result = 31 * result + trickLevel;
        return result;
    }


    {
        System.out.println("Создается объект Pet");
    }
    static {
        System.out.println("Загружается класс Pet");
    }

    protected void finalize() {
        System.out.println("Удаляется объект: " + this);
    }

}

