package com.company;

import java.util.Arrays;

public class Family {
    private enum Types {
        MIN("Family{mother=%s, father=%s, children=[]}"),
        MAX("Family{mother=%s, father=%s, children=%s, pet=%s}");



        String toStringText;
        Types(String toStringText) {this.toStringText = toStringText;}
    }

    private Types type;

    private Human mother, father;
    private Human[] children;
    private Pet pet;

    public Human getMother() {return mother;}
    public Human getFather() {return father;}
    public void setChildren(Human[] children) {this.children = children;}
    public Human[] getChildren() {return children;}
    public void setPet(Pet pet) {this.pet = pet;}
    public Pet getPet() {return pet;}
    public  int getChildrenCount(){return children.length;}

    private void constructor(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
    }

    public Family(Human mother, Human father) {
        constructor(mother, father);
        type = Types.MIN;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        constructor(mother, father);
        this.children = Arrays.copyOf(children, children.length);
        for(Human child : children) child.setFamily(this);
        this.pet = pet;
        type = Types.MAX;
    }


    @Override
    public String toString() {
        return type.equals(Types.MAX)
                ? String.format(Types.MAX.toStringText, mother.toString(), father.toString(), Arrays.toString(children), pet.toString())
                : String.format(Types.MIN.toStringText, mother.toString(), father.toString());
    }



    @Override
    public boolean equals(Object family){
        if (family == this){
            return true;
        }
        Family guests = (Family) family;
        if (this.countFamily() == guests.countFamily()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + mother.hashCode();
        result = 31 * result + father.hashCode();
        return result;
    }


    public void addChild(Human child) {
        this.children = Arrays.copyOf(children, children.length + 1);
        children[children.length-1] = child;
        child.setFamily(this);
    }
    public boolean deleteChild(int index) {
        if(index >= children.length || index < 0) return false;
        Human[] newChildren = new Human[children.length-1];
        for(int i  = 0; i < children.length - 1; i++) newChildren[i] = children[i < index ? i : i + 1];
        children[index].setFamily();
        children = newChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        for(int i = 0; i < children.length; i++) {
            if(children[i].equals(child)) return deleteChild(i);
        }
        return false;
    }

    public int countFamily() {return 2 + children.length;}

    {
        System.out.println("Создается объект Family");
    }
    static {
        System.out.println("Загружается класс Family");
    }

    protected void finalize() {
        System.out.println("Удаляется объект: " + this);
    }


}

