import com.company.*;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {


    Family getFamily() {
        HashMap<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "monday task");

        Human mother = new Human("Mother", "Mother Surname", 30, 100, schedule);
        Human father = new Human("Father", "Father Surname", 33, 90, schedule);
        Human firstChild = new Human("First", "Child", 1, 1, schedule);
        Human secondChild = new Human("Second", "Child", 2, 2, schedule);
        Family family = new Family(mother, father);
        family.addChild(firstChild);
        family.addChild(secondChild);

        return family;
    }

    @Test
    void testToString() {
        Family family = getFamily();

        assertEquals( "Family{mother=Human{name='Mother', surname='Mother Surname', year=30, iq=100, schedule=[ [monday: monday task] ]}, father=Human{name='Father', surname='Father Surname', year=33, iq=90, schedule=[ [monday: monday task] ]}, children=[]}", family.toString());
    }

    @Test
    void addChild() {
        Family family = getFamily();
        Human newChild = new Human("Third", "Child", 3, 3);
        family.addChild(newChild);
        assertEquals(family.getChildren().get(2), newChild);
    }

    @Test
    void testDeleteChildByIndex() {
        Family family = getFamily();
        int index = family.getChildren().size();
        family.deleteChild(1);
        assertEquals(index - 1, family.getChildren().size());
    }

    @Test
    void testDeleteChildByObj() {
        Family family = getFamily();
        Human deletedChild = new Human("Second", "Child", 2, 2);
        family.addChild(deletedChild);
        int firstSize = family.getChildren().size();
        family.deleteChild(deletedChild);
        assertEquals(firstSize - 1, family.getChildren().size());

    }


    @Test
    void countFamily() {
        Family family = getFamily();
        assertEquals(4, family.countFamily());
        family.addChild(new Human());
        assertEquals(5, family.countFamily());
    }
    @Test
    void testHashCodeAndEquals() {
        Family family1 = getFamily();
        Family family2 = family1;
        assertEquals(family1.hashCode(), family2.hashCode());
        assertTrue(family1.equals(family2));

    }
}

