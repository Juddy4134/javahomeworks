package com.company;

import java.util.Locale;

public enum Species {
    CAT(false, 4, true),
    DOG(false, 4, true),
    BIRD(true, 2, false),
    FISH(false, 0, false),
    TURTLE(false, 4, false),
    HAMSTER(false, 4, true);
    boolean canFly,
            hasFur;
    int numberOfLegs;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.hasFur = hasFur;
        this.numberOfLegs = numberOfLegs;
    }
    public String getValue() {return this.name().toLowerCase(Locale.ROOT);}
}

