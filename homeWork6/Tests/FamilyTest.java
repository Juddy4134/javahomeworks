import com.company.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    Family getFamily() {
        String[][] schedule = new String[][] {{DayOfWeek.TUESDAY.getValue(), "kino"}};
        Human mother = new Human("Mother", "Mother Surname", 30, 100, schedule);
        Human father = new Human("Father", "Father Surname", 33, 90, schedule);
        Human firstChild = new Human("First", "Child", 1, 1, schedule);
        Human secondChild = new Human("Second", "Child", 2, 2, schedule);
        Human[] children = new Human[] {firstChild, secondChild};
        return new Family(mother, father, children);
    }

    @Test
    void testToString() {
        Family family = getFamily();
       assertEquals("Family{mother=Human{name='Mother', surname='Mother Surname', year=30, iq=100, schedule=[[tuesday, kino]]}, father=Human{name='Father', surname='Father Surname', year=33, iq=90, schedule=[[tuesday, kino]]}, children=[Human{name='First', surname='Child', year=1, iq=1, schedule=[[tuesday, kino]]}, Human{name='Second', surname='Child', year=2, iq=2, schedule=[[tuesday, kino]]}], pet=bird{nickname='yellow bird', canFly=true, numberOfLegs=2, hasFur=false, age=100, trickLevel=100, habits=[eat nuts]}}", family.toString());
    }

    @Test
    void addChild() {
        Family family = getFamily();
        Human newChild = new Human("Third", "Child", 3, 3, new String[][] {{DayOfWeek.TUESDAY.getValue(), "kino"}});
        family.addChild(newChild);
        assertEquals(3, family.getChildren().length);
        assertEquals(family.getChildren()[2], newChild);
    }

    @Test
    void testDeleteChildByObj1() {
        Family family = getFamily();
        Human deletedChild = family.getChildren()[0];
        boolean result = family.deleteChild(deletedChild);
        assertEquals(1, family.getChildren().length);
        assertTrue(result);
    }

    @Test
    void testDeleteChildByObj2() {
        Family family = getFamily();
        Human deletedChild = new Human();
        boolean result = family.deleteChild(deletedChild);
        assertEquals(2, family.getChildren().length);
        assertFalse(result);
    }

    @Test
    void testDeleteChildById1() {
        Family family = getFamily();
        boolean result = family.deleteChild(1);
        assertEquals(1, family.getChildren().length);
        assertTrue(result);
    }

    @Test
    void testDeleteChildById2() {
        Family family = getFamily();
        boolean[] result = new boolean[] {family.deleteChild(2), family.deleteChild(-1)};
        assertEquals(2, family.getChildren().length);
        assertFalse(result[0]);
        assertFalse(result[1]);
    }

    @Test
    void countFamily() {
        Family family = getFamily();
        assertEquals(4, family.countFamily());
        family.addChild(new Human());
        assertEquals(5, family.countFamily());
    }
    @Test
    void testHashCodeAndEquals() {
        Family family1 = getFamily();
        Family family2 = family1;
        Family family3 = getFamily();
        assertEquals(family1.hashCode(), family2.hashCode());
        assertTrue(family1.equals(family2));

    }
}

