package com.company;

import java.text.ParseException;
import java.util.HashMap;
import com.company.Animals.Pet;

public final class Woman extends Human {
    public Woman() {
        super();
        gender = "Woman";
    }
    public Woman(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
        gender = "Woman";
    }
    public Woman(String name, String surname, String birthDate, int iq, HashMap<DayOfWeek, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
        gender = "Woman";
    }
    @Override
    public void greetPets() {
        for(Pet p : getFamily().getPets()) System.out.printf("Приветствую, %s\n", p.getNickname());
    }
    public void cook(String food) {
        System.out.printf("Я готовлю %s\n", food);
    }
}
