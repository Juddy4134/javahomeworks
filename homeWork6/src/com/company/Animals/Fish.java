package com.company.Animals;

import com.company.Species;;

public class Fish extends Pet {
    final Species type = Species.Fish;
    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Буль буль буль");
    }

    @Override
    public String toString() {
        return getToString(this, "abilities={canSwim, numberOfLegs=0}");
    }

    @Override
    public Species getType() {
        return type;
    }

}
