package com.company;

public final class Man extends Human {
    public Man() {
        super();
        gender = "Man";
    }
    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Man";
    }
    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Man";
    }
    @Override
    public void greetPet() {
        System.out.printf("Здраствуй, %s\n", getFamily().getPet().getNickname());
    }

    public void repairCar() {
        System.out.println("Что-то с ходовой, нужно заехать на СТО");
    }
}
