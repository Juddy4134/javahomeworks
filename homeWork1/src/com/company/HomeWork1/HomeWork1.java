package com.company.HomeWork1;

import java.util.Random;
import java.util.Scanner;

class Homework1 {
    public static  String[][] EventsArray = {
            {"1939", "When did the World War II begin?"},
            {"1945", "When did the World War II end?"},
            {"1914", "When did the World War I begin?"},
            {"1918", "When did the World War I end?"},
    };

    public static void main(String[] args) {
        int randomNum = new Random().nextInt(EventsArray.length);
        int year = Integer.parseInt(EventsArray[randomNum][0]);

        System.out.println("Enter your name: ");

        Scanner scanner = new Scanner(System.in);
        String userName = scanner.next();

        System.out.println("Let the game begin!");
        System.out.println(EventsArray[randomNum][1]);
        System.out.println("Enter your number:");

        while (true) {
            String userInput = scanner.next();
            int lastNumber;
            lastNumber = Integer.parseInt(userInput);

            if (lastNumber == year) {
                System.out.println("Congratulations, " + userName + "!");
                break;
            } else if (lastNumber > year) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Your number is too small. Please, try again.");
            }
        }
    }

}
