package com.company;

public enum Species {
    UNKNOWN,
    Fish,
    DomesticCat,
    Dog,
    RoboCat;

    }

