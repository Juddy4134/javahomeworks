package com.company;

import java.util.HashMap;
import com.company.Animals.Pet;

public final class Woman extends Human {
    public Woman() {
        super();
        gender = "Woman";
    }
    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Woman";
    }
    public Woman(String name, String surname, int year, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Woman";
    }
    @Override
    public void greetPets() {
        for(Pet p : getFamily().getPets()) System.out.printf("Приветствую, %s\n", p.getNickname());
    }
    public void cook(String food) {
        System.out.printf("Я готовлю %s\n", food);
    }
}
