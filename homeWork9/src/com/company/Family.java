package com.company;


import com.company.Animals.Pet;
import java.util.ArrayList;
import java.util.HashSet;

public class Family {
    private enum Types {
        MIN("Family{mother=%s, father=%s, children=[]}"),
        MAX("Family{mother=%s, father=%s, children=%s, pet=%s}");

        String toStringText;
        Types(String toStringText) {this.toStringText = toStringText;}
    }

    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();
    private Types type;

    public Human getMother() {return mother;}
    public Human getFather() {return father;}
    public void setChildren(ArrayList<Human> children) {this.children = children;}
    public ArrayList<Human> getChildren() {return children;}
    public void setPets(HashSet<Pet> pets) {this.pets = pets;}
    public HashSet<Pet> getPets() {return pets;}
    public void addPet (Pet pet){this.pets.add(pet);}

    private void constructor(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        type = Types.MIN;
    }

    public Family(Human mother, Human father) {
        constructor(mother, father);
    }

    public Family(Human mother, Human father, ArrayList<Human> children, HashSet<Pet> pets) {
        constructor(mother, father);
        this.children = children;
        for(Human child : children) child.setFamily(this);
        this.pets = pets;
        type = Types.MAX;
    }

    @Override
    public String toString() {
        String childrenToString = "[ ";
        for(Human child : children) {
            childrenToString += "<" + child + "> ";
        }
        childrenToString += " ]";
        String petsToString = "[ ";
        for(Pet pet : pets) {
            petsToString += "<" + pet + "> ";
        }
        petsToString += " ]";
        return type.equals(Types.MAX)
                ? String.format(Types.MAX.toStringText, mother.toString(), father.toString(), childrenToString, petsToString)
                : String.format(Types.MIN.toStringText, mother.toString(), father.toString());
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }
    public boolean deleteChild(int index) {
        if(index >= children.size() || index < 0) return false;
        children.get(index).setFamily();
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        if (children.contains(child)){
            deleteChild(children.indexOf(child));
            return true;
        } else {
            return false;
        }
    }

    public int countFamily() {return 2 + children.size();}

    @Override
    public boolean equals(Object family){
        if (family == this){
            return true;
        }
        Family guests = (Family) family;
        if (this.countFamily() == guests.countFamily()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + mother.hashCode();
        result = 31 * result + father.hashCode();
        return result;
    }
//
//    {
//        System.out.println("Создается объект Family");
//    }
//    static {
//        System.out.println("Загружается класс Family");
//    }
//
//    protected void finalize() {
//        System.out.println("Удаляется объект: " + this);
//    }

}





