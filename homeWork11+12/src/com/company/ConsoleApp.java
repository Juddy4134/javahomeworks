package com.company;

import com.company.Animals.Dog;
import com.company.Animals.Fish;
import com.company.Animals.Pet;
import com.company.Animals.RoboCat;
import com.company.DAO.FamilyController;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

public class ConsoleApp {
    private final FamilyController familyControllerToDisplay = new FamilyController();
    private final String mainMenu =
            """
                    - 0. Завершить работу\s
                    - 1. Заполнить тестовыми данными\s
                    - 2. Отобразить весь список семей\s
                    - 3. Отобразить список семей, где количество людей больше заданного\s
                    - 4. Отобразить список семей, где количество людей меньше заданного\s
                    - 5. Подсчитать количество семей, где количество членов равно\s
                    - 6. Создать новую семью\s
                    - 7. Удалить семью по индексу семьи в общем списке\s
                    - 8. Редактировать семью по индексу семьи в общем списке\s
                    - 9. Удалить всех детей старше возраста\s
                    """;

    private final String subMenuForEditFamily =
            """
                    - 1. Родить ребенка\s
                    - 2. Усыновить ребенка\s
                    - 3. Вернуться в главное меню\s
                    """;

    public void startApp() {
        System.out.print(mainMenu);
        processUserInput();
    }

    private void processUserInput() {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Choose menu item: ");
        Map<Integer, Runnable> map = new HashMap<>();
        map.put(1, () -> {
            try {
                fillTableWithTestData();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        map.put(2, this::displayAllFamiliesWithIndexes);

        map.put(3, this::displayAllFamiliesBiggerThan);

        map.put(4, this::displayAllFamiliesLessThan);

        map.put(5, this::countAllFamiliesWhereNumberOfMembersEqualTo);

        map.put(6, () -> {
            try {
                createNewFamilyAndAddItToList();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        map.put(7, this::deleteFamilyFromList);

        map.put(8, () -> {
            try {
                editFamilyFromListByIndex();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        map.put(9, this::deleteAllChildrenOlderThanInAllFamilies);

        map.put(0, () -> showContextMenu("Exit"));

        while (true) {
            String menuItem = userInput.nextLine();

            if (isNumeric(menuItem) &&
                    Integer.parseInt(menuItem) > 0 &&
                    Integer.parseInt(menuItem) <= 11
            ) {
                map.get(Integer.parseInt(menuItem)).run();
            } else if (isNumeric(menuItem) && Integer.parseInt(menuItem) == 0) {
                break;
            } else {
                System.out.println("Wrong data! Enter number between 1 and 11. Or 0 to exit!");
            }
        }
    }

    private void fillTableWithTestData() throws ParseException {

        HashMap<DayOfWeek, String> schedule = new HashMap<>();

        schedule.put(DayOfWeek.MONDAY, "task1");
        schedule.put(DayOfWeek.FRIDAY, "task2");

        //Family1

        HashSet<String> fishHabits = new HashSet<>();
        fishHabits.add("swim");
        fishHabits.add("eat");
        Fish fish1 = new Fish("King", 5, 40, fishHabits);

        HashSet<String> catHabits = new HashSet<>();
        catHabits.add("jump");
        catHabits.add("meow");
        Fish cat1 = new Fish("Frank", 5, 80, catHabits);

        HashSet<Pet> petsForFamily1 = new HashSet<>();
        petsForFamily1.add(fish1);
        petsForFamily1.add(cat1);

        Man father1 = new Man("Frank", "Sinatra", "11/11/1975", 199, schedule);
        Woman mother1 = new Woman("Nikki", "Sinatra", "11/11/1965", 199, schedule);

        Man kid11 = new Man("John", "Sinatra", "11/11/1995", 199, schedule);
        Woman kid12 = new Woman("Mary", "Sinatra", "11/11/1992", 199, schedule);

        ArrayList<Human> kidsList1 = new ArrayList<>();
        kidsList1.add(kid11);
        kidsList1.add(kid12);

        Family f1 = new Family(mother1, father1);
        f1.setPets(petsForFamily1);
        f1.setChildren(kidsList1);

        //Family2

        HashSet dogHabits = new HashSet<String>();
        dogHabits.add("Верный помошник");


        Woman Marge = new Woman("Мардж", "Симпсон", "09/02/1980", 120, schedule);
        Man Homer = new Man("Гомер", "Симпсон", "15/01/1970", 50, schedule);
        Human Liza = new Human("Лиза","Simpson","12/03/2010",120);
        Dog dog = new Dog("Маленький помошник Санты", 2, 40, dogHabits);

        HashSet SimpsonsPets = new HashSet<Pet>();
        SimpsonsPets.add(dog);

        ArrayList<Human> kidsList2 = new ArrayList<>();
        kidsList2.add(Liza);

        Family f2 = new Family(Marge, Homer);
        f2.setPets(SimpsonsPets);
        f2.setChildren(kidsList2);

        //Family3

        HashSet<String> robotHabits = new HashSet<>();
        robotHabits.add("walk");
        robotHabits.add("recharge");
        RoboCat robot3 = new RoboCat("king", 5, 90, robotHabits);

        HashSet<String> dogHabits3 = new HashSet<>();
        dogHabits3.add("jump");
        dogHabits3.add("meow");
        Dog dog3 = new Dog("Frank", 5, 80, dogHabits3);

        HashSet<Pet> petsForFamily3 = new HashSet<>();
        petsForFamily3.add(robot3);
        petsForFamily3.add(dog3);

        Man father3 = new Man("Mike", "Peterson", "11/11/1970", 199, schedule);
        Woman mother3 = new Woman("Angela", "Peterson", "11/11/1965", 199, schedule);

        Man kid31 = new Man("Peter", "Peterson", "11/11/1995", 199, schedule);
        Man kid32 = new Man("Jack", "Peterson", "11/11/1996", 199, schedule);
        Woman kid33 = new Woman("Monika", "Peterson", "11/11/1992", 199, schedule);

        ArrayList<Human> kidsList3 = new ArrayList<>();
        kidsList3.add(kid31);
        kidsList3.add(kid32);
        kidsList3.add(kid33);

        Family f3 = new Family(mother3, father3);
        f3.setPets(petsForFamily3);
        f3.setChildren(kidsList3);


        List<Family> fs = new ArrayList<>();
        fs.add(f1);
        fs.add(f2);
        fs.add(f3);


        this.familyControllerToDisplay.setFamilies(fs);
        showContextMenu("Family list was successfully filled");
    }

    private void displayAllFamiliesWithIndexes() {
        if (isDBEmpty()) {
            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            StringBuilder displayInfo = new StringBuilder();

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                displayInfo.append("Index Of Family ").append(familyIndex).append("\n").append(result.get(i).prettyFormat()).append("\n").append("////////////////////////////////////////////////////////////////////////////////////////\n");
            }

            System.out.println(displayInfo);

            showContextMenu("All families are displayed with Indexes!");
        }
    }

    private void displayAllFamiliesBiggerThan() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            StringBuilder displayInfo = new StringBuilder();

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                if (result.get(i).countFamily() > numberOfMembersInFamily)
                    displayInfo.append("Index Of Family ").append(familyIndex).append("\n").append(result.get(i).prettyFormat()).append("\n").append("////////////////////////////////////////////////////////////////////////////////////////\n");
            }

            System.out.println(displayInfo.toString().equals("") ? "There are no such families!" : displayInfo.toString());

            showContextMenu("All families that greater than " + numberOfMembersInFamily + " are displayed!");
        }
    }

    private void displayAllFamiliesLessThan() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            StringBuilder displayInfo = new StringBuilder();

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                if (result.get(i).countFamily() < numberOfMembersInFamily)
                    displayInfo.append("Index Of Family ").append(familyIndex).append("\n").append(result.get(i).prettyFormat()).append("\n").append("////////////////////////////////////////////////////////////////////////////////////////\n");
            }

            System.out.println(displayInfo.toString().equals("") ? "There are no such families!" : displayInfo.toString());

            showContextMenu("All families that less than " + numberOfMembersInFamily + " are displayed!");
        }
    }

    private void countAllFamiliesWhereNumberOfMembersEqualTo() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();

            int numberOfFamilies = 0;

            for (Family family : result) {
                if (family.countFamily() == numberOfMembersInFamily) {
                    numberOfFamilies++;
                }
            }

            showContextMenu(numberOfFamilies + " Family where number of members are equal to " + numberOfMembersInFamily);
        }
    }

    private void createNewFamilyAndAddItToList() throws ParseException {
        Human[] humans = new Human[2];
        String[] humanData;

        for (int i = 0; i < humans.length; i++) {
            String info = i == 0 ? "Enter mother data:\n" : "Enter father data:\n";
            System.out.println(info);

            humanData = newHumanData();

            int day = Integer.parseInt(humanData[2]);
            int month = Integer.parseInt(humanData[3]);
            int year = Integer.parseInt(humanData[4]);
            int iq = Integer.parseInt(humanData[5]);

            humans[i] = new Human(humanData[0], humanData[1], String.format("%d/%d/%d", day, month, year), iq);

        }

        familyControllerToDisplay.createNewFamily(humans[0], humans[1]);

    }

    private void deleteFamilyFromList() {
        if (isDBEmpty()) {
            System.out.println("Enter index to delete family. Number of items " + familyControllerToDisplay.count());

            int indexToDelete = processingInputOfFamilyIndex();

            familyControllerToDisplay.deleteFamilyByIndex(indexToDelete);

            showContextMenu("Item removed.");
        }
    }

    private void editFamilyFromListByIndex() throws ParseException {
        if (isDBEmpty()) {
            System.out.println(subMenuForEditFamily);

            Scanner userInput = new Scanner(System.in);
            boolean subMenuFlag = true;

            while (subMenuFlag) {
                String item = userInput.nextLine();
                switch (item) {
                    case "1":
                        try {
                            bornChild();
                        } catch (FamilyOverflowException err) {
                            System.out.println(err.getMessage());
                            System.out.println(subMenuForEditFamily);
                        }
                        break;
                    case "2":
                        try {
                            adoptChild();
                        } catch (FamilyOverflowException err) {
                            System.out.println(err.getMessage());
                            System.out.println(subMenuForEditFamily);
                        }
                        break;
                    case "3":
                        subMenuFlag = false;
                        System.out.println(mainMenu);
                        break;
                    default:
                        System.out.println("Wrong input only 1, 2, 3 allowed!");
                }
            }

        }
    }

    private void bornChild() throws ParseException {
        System.out.println("Born Kid Data:");

        extractDataFromHuman(true);
        System.out.println("Child born!");

    }

    private void adoptChild() throws ParseException {
        System.out.println("Adopted Kid Data:");

        extractDataFromHuman(false);
        System.out.println("Child adopted!");
    }

    private void deleteAllChildrenOlderThanInAllFamilies() {
        if (isDBEmpty()) {
            Scanner userInput = new Scanner(System.in);

            int ageOfChildren = processingInputOfHumanAge();

            familyControllerToDisplay.deleteAllChildrenOlderThen(ageOfChildren);
            showContextMenu("All kids older than " + ageOfChildren + " year deleted!");
        }
    }


    // Methods for prettier format
    private int processingInputOfFamilyMembersNumber() {
        Scanner userInput = new Scanner(System.in);
        int membersNumber;
        System.out.println("Enter Number Of Members: ");

        while (true) {
            String numberOfFamilyMembers = userInput.nextLine();

            if (isNumeric(numberOfFamilyMembers)) {
                if (Integer.parseInt(numberOfFamilyMembers) >= 2) {
                    membersNumber = Integer.parseInt(numberOfFamilyMembers);
                    break;
                } else {
                    System.out.println("Enter number bigger than 2");
                }
            } else {
                System.out.println("Enter integer!");
            }
        }

        return membersNumber;
    }

    private int processingInputOfFamilyIndex() {
        int boundary = familyControllerToDisplay.count();
        Scanner userInput = new Scanner(System.in);
        int index ;
        int border = boundary - 1;
        System.out.println("Enter Index: ");

        while (true) {
            String familyIndex = userInput.nextLine();

            if (isNumeric(familyIndex) &&
                    Integer.parseInt(familyIndex) >= 0 &&
                    Integer.parseInt(familyIndex) < boundary
            ) {
                index = Integer.parseInt(familyIndex);
                break;
            } else {
                System.out.println("Enter a Number within 0 and " + border );
            }
        }

        return index;
    }

    private int processingInputOfHumanAge() {
        Scanner userInput = new Scanner(System.in);
        int age ;
        System.out.println("Enter Age: ");

        while (true) {
            String numberOfFamilyMembers = userInput.nextLine();

            if (isNumeric(numberOfFamilyMembers) && Integer.parseInt(numberOfFamilyMembers) > 0) {
                age = Integer.parseInt(numberOfFamilyMembers);
                break;
            } else {
                System.out.println("Enter integer greater than 0!");
            }
        }

        return age;
    }

    private String[] newHumanData() {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Enter name:");
        String name = userInput.nextLine();
        System.out.println("Enter surname:");
        String surname = userInput.nextLine();

        System.out.println("Enter birth year yyyy:");
        String birthYear ;
        while (true) {
            String birthYYYY = userInput.nextLine();

            if (isNumeric(birthYYYY) && birthYYYY.length() == 4) {
                birthYear = birthYYYY;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter birth month mm:");
        String birthMonth ;
        while (true) {
            String birthMM = userInput.nextLine();

            if (isNumeric(birthMM) && birthMM.length() == 2
                    && (Integer.parseInt(birthMM) >= 1 && Integer.parseInt(birthMM) <= 12)) {
                birthMonth = birthMM;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter birth day dd:");
        String birthDay ;
        while (true) {
            String birthDD = userInput.nextLine();

            if (isNumeric(birthDD) && birthDD.length() == 2
                    && (Integer.parseInt(birthDD) >= 1 && Integer.parseInt(birthDD) <= 31)) {
                birthDay = birthDD;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter iq level:");
        String iq ;
        while (true) {
            String iqLevel = userInput.nextLine();

            if (isNumeric(iqLevel)) {
                iq = iqLevel;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        return new String[]{name, surname, birthDay, birthMonth, birthYear, iq};
    }

    private boolean isDBEmpty() {
        if (familyControllerToDisplay.getAllFamilies().size() == 0) {
            System.out.println("Database is empty!");
            return false;
        }

        return true;
    }

    private void confirmChangesToDB(int index, Human kid) {
        if (familyControllerToDisplay.getAllFamilies().get(index).countFamily() > 5) {
            throw new FamilyOverflowException("Maximum Reached");
        }

        ArrayList<Human> children = new ArrayList<>();
        if (familyControllerToDisplay.getAllFamilies().get(index).getChildren() != null)
            children.addAll(familyControllerToDisplay.getAllFamilies().get(index).getChildren());
        children.add(kid);

        familyControllerToDisplay.getAllFamilies().get(index).setChildren(children);
    }

    private void extractDataFromHuman(boolean isChildNew) throws ParseException {
        String[] humanData = newHumanData();

        int day = Integer.parseInt(humanData[2]);
        int month = Integer.parseInt(humanData[3]);
        int year = Integer.parseInt(humanData[4]);
        int iq = Integer.parseInt(humanData[5]);


        System.out.println("Family index, please. Number of families " + familyControllerToDisplay.getAllFamilies().size());
        int index = processingInputOfFamilyIndex();

        if (isChildNew) {
            LocalDate currentDate = LocalDate.now();
            day = currentDate.getDayOfMonth();
            month = currentDate.getMonthValue();
            year = currentDate.getYear();
        }

        Human kid = new Human(humanData[0], humanData[1], String.format("%d/%d/%d", day, month, year), iq);


        confirmChangesToDB(index, kid);


        System.out.println(subMenuForEditFamily);
    }

    private static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }

        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }

    private void showContextMenu(String msg) {
        System.out.println(mainMenu);
        System.out.println(msg);
        System.out.println("Choose menu item: ");
    }

}

