package com.company.DAO;


import com.company.Family;


import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<Family>();

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {

        families.forEach(Family::toString);

        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index > families.size()) return null;

        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index > families.size()) return false;
        families.remove(index);

        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!families.contains(family)) return false;
        deleteFamily(families.indexOf(family));

        return true;
    }

    @Override
    public List<Family> saveFamily(Family family) {

        int index = families.indexOf(family);

        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }

        return families;
    }

}
