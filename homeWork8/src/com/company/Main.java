package com.company;

import com.company.Animals.*;
import com.company.DAO.FamilyController;
import com.company.DAO.FamilyService;

import java.util.HashSet;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashSet dogHabits = new HashSet<String>();
        dogHabits.add("Верный помошник");

        HashMap<DayOfWeek, String> map = new HashMap<>();
        map.put(DayOfWeek.MONDAY, "monday task");
        map.put(DayOfWeek.TUESDAY, "tuesday task");
        map.put(DayOfWeek.WEDNESDAY, "wednesday task");
        map.put(DayOfWeek.THURSDAY, "thursday task");
        map.put(DayOfWeek.FRIDAY, "friday task");
        map.put(DayOfWeek.SATURDAY, "saturday task");
        map.put(DayOfWeek.SUNDAY, "sunday task");

        Woman Marge = new Woman("Мардж", "Симпсон", 35, 120, map);
        Man Homer = new Man("Гомер", "Симпсон", 39, 50, map);
        Human Liza = new Human("Лиза","Simpson",12,120);
        Dog dog = new Dog("Маленький помошник Санты", 2, 40, dogHabits);

        HashSet SimpsonsPets = new HashSet<Pet>();
        SimpsonsPets.add(dog);

        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(Marge,Homer);
        familyService.bornChild(familyService.getFamilyById(0),Liza);
        familyService.addPet(dog,0);
        System.out.println(familyService.getAllFamilies());
        System.out.println(familyService.getPets(0));
    }

}

