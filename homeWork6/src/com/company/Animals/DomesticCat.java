package com.company.Animals;

import com.company.Species;

public class DomesticCat extends Pet implements CanDoFool {
    public final Species type = Species.DomesticCat;

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Мяу");
    }

    @Override
    public String toString() {
        return getToString(this, "abilities={numberOfLegs=4, hasFur}");
    }

    @Override
    public Species getType() {
        return type;
    }

    @Override
    public void foul() {
        System.out.println(getNickname() + " сделал(а) пакость, и теперь хитро мурчит");
    }
}
