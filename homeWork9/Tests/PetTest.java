import com.company.Animals.*;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    HashSet<String> catHabits = new HashSet<>();

    @Test
    void testToString() {
        catHabits.add("bip-bip");
        String expected = "cat{nickname='Name', canFly=false, numberOfLegs=4, hasFur=true, age=1, trickLevel=100, habits=[love sleep near door, and on table too]}";
        RoboCat newPet = new RoboCat( "Name", 1, 100,catHabits);
        assertEquals(expected, newPet.toString());
    }
    @Test
    void testHashCodeAndEquals() {
        RoboCat pet1 = new RoboCat( "Name", 1, 100,catHabits);
        Pet pet2 = pet1;
        assertEquals(pet1.hashCode(), pet2.hashCode());
        assertTrue(pet1.equals(pet2));
    }
}

