import com.company.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() {
        String expected = "Human{name='Name', surname='Surname', year=1, iq=2}";
        Human newHuman = new Human("Name", "Surname", 1, 2);
        assertEquals(expected, newHuman.toString());
    }
    @Test
    void testHashCodeAndEquals() {
        Human human1 = new Human();
        Human human2 = human1;
        assertEquals(human1.hashCode(), human2.hashCode());
        assertTrue(human1.equals(human2));
    }
}

