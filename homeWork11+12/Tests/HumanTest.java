import com.company.*;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() throws ParseException {
        String expected = "Human{name='Name', surname='Surname', year=20, iq=2}";
        Human newHuman = new Human("Name", "Surname", "09/02/2001", 2);
        assertEquals(expected, newHuman.toString());
    }
    @Test
    void testHashCodeAndEquals() {
        Human human1 = new Human();
        Human human2 = human1;
        assertEquals(human1.hashCode(), human2.hashCode());
        assertTrue(human1.equals(human2));
    }
}

