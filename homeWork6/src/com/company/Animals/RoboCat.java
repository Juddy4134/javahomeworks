package com.company.Animals;

import com.company.Species;

public class RoboCat extends Pet {
    public final Species type = Species.RoboCat;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("би бип мяу");
    }

    @Override
    public String toString() {
        return getToString(this, "abilities={robot, numberOfLegs=4}");
    }

    @Override
    public Species getType() {
        return type;
    }

}
