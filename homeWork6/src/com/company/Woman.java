package com.company;

public final class Woman extends Human {
    public Woman() {
        super();
        gender = "Woman";
    }
    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Woman";
    }
    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Woman";
    }
    @Override
    public void greetPet() {
        System.out.printf("Приветствую, %s\n", getFamily().getPet().getNickname());
    }
    public void cook(String food) {
        System.out.printf("Я готовлю %s\n", food);
    }
}
