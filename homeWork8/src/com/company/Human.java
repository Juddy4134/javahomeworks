package com.company;

import java.util.*;
import java.util.Random;
import static java.util.Objects.isNull;
import com.company.Animals.Pet;

public class Human {
    private String name, surname;
    private int age, iq;
    private HashMap<DayOfWeek, String> schedule;
    private Family family;
    private boolean isEmpty = false;
    protected String gender = "Human";

    public void setName(String name) {this.name = name;}
    public String getName() {return name;}
    public void setSurname(String surname) {this.surname = surname;}
    public String getSurname() {return surname;}
    public void setAge(int year) {this.age = age;}
    public int getAge() {return age;}
    public void setIq(int iq) {this.iq = iq;}
    public int getIq() {return iq;}
    public void setSchedule(HashMap<DayOfWeek, String> schedule) {this.schedule = schedule;}
    public HashMap<DayOfWeek, String> getSchedule() {return schedule;}
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setFamily() {this.family = null;}
    public Family getFamily() {return family;}

    public Human() {
        isEmpty = true;
    }
    private void constructor(String name, String surname, int age, int iq) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.iq = iq;
    }

    public Human(String name, String surname, int age, int iq) {
        constructor(name, surname, age, iq);
    }

    public Human(String name, String surname, int age, int iq, HashMap<DayOfWeek, String> schedule) {
        constructor(name, surname, age, iq);
        this.schedule = schedule;
    }

    public void greetPets() {
        for(Pet p : family.getPets()) System.out.printf("Привет, %s\n", p.getNickname());
    }
    public void describePets() {
        for(Pet p : family.getPets()) System.out.printf("У меня есть %s, ему %d лет, он %s\n", p.getType(), p.getAge(), p.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
    }
    @Override
    public String toString() {
        if(isEmpty) return gender+"{}";
        String toStringText = String.format("%s{name='%s', surname='%s', year=%d, iq=%d", gender, name, surname, age, iq);
        if(schedule == null) return toStringText + "}";
        String scheduleToString = "[";
        for(Map.Entry<DayOfWeek, String> day : schedule.entrySet()) {
            scheduleToString += String.format(" [%s: %s] ", day.getKey().getValue(), day.getValue());
        }
        scheduleToString += "]";
        return toStringText + ", schedule=" + scheduleToString + "}";
    }

    public ArrayList<Boolean> feedPets(boolean isItNeedToFeed) {
        ArrayList<Boolean> results = new ArrayList<>();
        for(Pet p : family.getPets()) {
            Random random = new Random();
            if(isItNeedToFeed || p.getTrickLevel() > random.nextInt(101)) {
                System.out.println("Хм... покормлю ка я " + p.getNickname());
                results.add(true);
            } else {
                System.out.printf("Думаю, %s не голоден.\n", p.getNickname());
                results.add(true);
            }
        }
        return results;
    }

    @Override
    public boolean equals(Object person){
        if (person == this){
            return true;
        }
        if (isNull(person) && isNull(this)){
            return true;
        }
        Human guest = (Human) person;
        if (this.getName().equals(guest.getName()) && this.getAge() == guest.getAge()){
            return true;
        } else {
            return false;
        }

    }

    public int hashCode() {
        if (this.isEmpty){
            return 1;
        }
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result +  surname.hashCode();
        result = 31 * result + age;
        result = 31 * result + iq;
        return result;
    }



//
//    {
//        System.out.println("Создается объект Human");
//    }
//    static {
//        System.out.println("Загружается класс Human");
//    }
//
//    protected void finalize() {
//        System.out.println("Удаляется объект: " + this);
//    }

}

