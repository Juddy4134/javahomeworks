package com.company.Animals;

import com.company.Species;

public class Dog extends Pet implements CanDoFool {
    public final Species type = Species.Dog;

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Гав гав");
    }

    @Override
    public String toString() {
        return getToString(this, "abilities={hasFur, numberOfLegs=4}");
    }

    @Override
    public Species getType() {
        return type;
    }

    @Override
    public void foul() {
        System.out.println(getNickname() + " сделал(а) пакость, и теперь хитро виляет хвостом");
    }
}
