package com.company.DAO;

import com.company.Animals.Pet;
import com.company.Family;
import com.company.Human;


import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public CollectionFamilyDao getFamilyDao() {
        return familyDao;
    }

    public void setFamilyDao(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getFamilies();
    }

    public List<Family> displayAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familyMembersLength) {
        return familyDao.getFamilies().stream().filter(item -> item.countFamily() > familyMembersLength).collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int familyMembersLength) {
        return familyDao.getFamilies().stream().filter(item -> item.countFamily() < familyMembersLength).collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int familyMembers) {
        return (int) familyDao.getFamilies()
                .stream()
                .filter(item -> item.countFamily() == familyMembers)
                .count();
    }

    public List<Family> createNewFamily(Human mother, Human father) {
        List<Family> res = familyDao.getFamilies();

        Family newFamily = new Family(mother, father);

        res.add(newFamily);
        return res;
    }

    public void deleteFamilyByIndex(int index) {
        if (index < 0 || index > familyDao.getFamilies().size()) {
            System.out.println("Wrong Index!!!");
        } else {
            familyDao.getFamilies().remove(index);
        }
    }

    public Family bornChild(Family family, Human child)  {
        int index = familyDao.getFamilies().indexOf(family);
        if (index < 0) return family;

        familyDao.getFamilies().get(index).addChild(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        int index = familyDao.getFamilies().indexOf(family);
        if (index < 0) return family;

        familyDao.getFamilyByIndex(index).addChild(child);
        return family;
    }

    public List<Family> deleteAllChildrenOlderThen(int age) {
        return familyDao.getFamilies().stream()
                .peek(f -> {
                    if (f.getChildren() != null)
                        f.getChildren().removeIf(it -> it.getBirthdayYear() >= age);
                })
                .collect(Collectors.toList());
    }

    public int count() {
        return familyDao.getFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (index < 0 || index > familyDao.getFamilies().size()) return null;
        return familyDao.getFamilies().get(index);
    }

    public Set<Pet> getPets(int index) {
        if (index < 0 || index > familyDao.getFamilies().size()) return null;
        return familyDao.getFamilies().get(index).getPets();
    }

    public void addPet(Pet pet, int index) {
        boolean isIndexCorrect = true;
        if (index < 0 || index > familyDao.getFamilies().size()) isIndexCorrect = false;

        if (isIndexCorrect) {
            familyDao.getFamilies().get(index).getPets().add(pet);
        }
    }

}
