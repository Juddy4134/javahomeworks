package com.company.DAO;


import com.company.Family;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<>();
    {loadData();}

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
        saveData();
    }

    @Override
    public List<Family> getAllFamilies() {

        families.forEach(Family::toString);

        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index > families.size()) return null;

        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index > families.size()) return false;
        families.remove(index);
        saveData();
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!families.contains(family)) return false;
        deleteFamily(families.indexOf(family));
        saveData();
        return true;
    }

    @Override
    public List<Family> saveFamily(Family family) {

        int index = families.indexOf(family);

        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }
        saveData();
        return families;
    }

    private void saveData()  {
        try {
            FileOutputStream fos = new FileOutputStream("src/com/company/DAO/FamilyDB.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(families);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        try {
            try {
                FileInputStream fis = new FileInputStream("src/com/company/DAO/FamilyDB.txt");
                ObjectInputStream ois = new ObjectInputStream(fis);
                try {
                    families = (ArrayList<Family>) ois.readObject();
                } catch (ClassNotFoundException e){
                    e.printStackTrace();
                }
            } catch (EOFException e) {
                e.printStackTrace();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
