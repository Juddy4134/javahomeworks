package com.company;

public class Main {
    public static void main(String[] args) {
        Human Marge = new Human("Мардж", "Симпсон", 35, 120, new String[][] {{"Понедельник", "Уборка дома"}, {"Вторник", "Закупка еды"}});
        Human Homer = new Human("Гомер", "Симпсон", 39, 50, new String[][] {{"Все дни", "Пить пиво после работы"}});
        Human Bart = new Human("Барт", "Симпсон", 10, 90, new String[][] {{"Все дни", "Доставать Лизу"}});
        Human Liza = new Human("Лиза", "Симпсон", 8, 140, new String[][] {{"Все дни", "Учится и помогать маме"}});
        Pet dog = new Pet(Species.DOG, "Маленький помошник Санты", 2, 40, new String[] {"Верный помошник"});

        Family newFamily = new Family(Marge, Homer, new Human[] {Bart, Liza}, dog);
        System.out.println(newFamily.toString());

        System.out.printf("Количество людей в семье: %d\n", newFamily.countFamily());
        System.out.println("До первого удаления:");

        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }
        System.out.printf("%s equals to %s - %b\n",Marge.getName(),Liza.getName(),Marge.equals(Liza));

        Human Meggi = new Human("Мэгги", "Симпсон", 1, 60, new String[][] {{"Все дни", "Сосать соску"}});
        newFamily.addChild(Meggi);
        newFamily.deleteChild(1);
        System.out.println("После первого удаления:");
        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }
        newFamily.deleteChild(Bart);
        System.out.println("После второго удаления:");
        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }

        Meggi.greetPet();
        Homer.describePet();
        Marge.feedPet(false);
    }

}

