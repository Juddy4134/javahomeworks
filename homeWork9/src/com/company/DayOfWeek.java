package com.company;

import java.util.Locale;

public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public String getValue() {return this.name().toLowerCase(Locale.ROOT);}
}

