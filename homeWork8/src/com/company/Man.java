package com.company;

import java.util.HashMap;
import com.company.Animals.Pet;

public final class Man extends Human {
    public Man() {
        super();
        gender = "Man";
    }
    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Man";
    }
    public Man(String name, String surname, int year, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Man";
    }
    @Override
    public void greetPets() {
        for(Pet p : getFamily().getPets()) System.out.printf("Здраствуй, %s\n", p.getNickname());
    }

    public void repairCar() {
        System.out.println("Что-то с ходовой, нужно заехать на СТО");
    }
}
