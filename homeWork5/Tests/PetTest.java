import com.company.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        String expected = "cat{nickname='Name', canFly=false, numberOfLegs=4, hasFur=true, age=1, trickLevel=100, habits=[love sleep near door, and on table too]}";
        Pet newPet = new Pet(Species.CAT, "Name", 1, 100, new String[] {"love sleep near door", "and on table too"});
        assertEquals(expected, newPet.toString());
    }
    @Test
    void testHashCodeAndEquals() {
        Pet pet1 = new Pet();
        Pet pet2 = pet1;
        assertEquals(pet1.hashCode(), pet2.hashCode());
        assertTrue(pet1.equals(pet2));
    }
}

