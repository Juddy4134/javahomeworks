package com.company.Animals;

import java.util.*;
import com.company.Species;

public abstract class Pet {
    private String nickname;
    private String[] habits;
    private int age,
            trickLevel;

    public void setNickname(String nickname) {this.nickname = nickname;}
    public String getNickname() {return nickname;}
    public void setHabits(String[] habits) {this.habits = habits;}
    public String[] getHabits() {return habits;}
    public void setAge(int age) {this.age = age;}
    public int getAge() {return age;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public int getTrickLevel() {return trickLevel;}

    public Pet(String nickname, int age,int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = Arrays.copyOf(habits, habits.length);
    }

    public static void eat() {
        System.out.println("Я кушаю!");
    }
    public abstract void respond();
    @Override
    public abstract String toString();

//    {
//        System.out.println("Создается объект Pet");
//    }
//    static {
//        System.out.println("Загружается класс Pet");
//    }
//
//    protected void finalize() {
//        System.out.println("Удаляется объект: " + this);
//    }
    protected static String getToString(Pet link, String special) {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s, %s}", link.getType(), link.getNickname(), link.getAge(), link.getTrickLevel(), Arrays.toString(link.getHabits()), special);
    }
    public abstract Species getType();
}