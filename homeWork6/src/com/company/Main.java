package com.company;

import com.company.Animals.*;

public class Main {
    public static void main(String[] args) {
        Woman Marge = new Woman("Мардж", "Симпсон", 35, 120, new String[][] {{"Понедельник", "Уборка дома"}, {"Вторник", "Закупка еды"}});
        Man Homer = new Man("Гомер", "Симпсон", 39, 50, new String[][] {{"Все дни", "Пить пиво после работы"}});
        Dog dog = new Dog("Маленький помошник Санты", 2, 40, new String[] {"Верный помошник"});
        System.out.println(Marge.toString());
        System.out.println(Homer.toString());
        System.out.println(dog.toString());
    }

}

