package com.company;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String errorMessage) {
        super(errorMessage);
    }
}

