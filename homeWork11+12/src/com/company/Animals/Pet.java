package com.company.Animals;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import com.company.Species;
import java.util.HashSet;

public abstract class Pet implements Serializable {
    private String nickname;
    private int age,
            trickLevel;
    private HashSet<String> habits;

    public void setNickname(String nickname) {this.nickname = nickname;}
    public String getNickname() {return nickname;}
    public void setHabits(HashSet<String> habits) {this.habits = habits;}
    public HashSet<String> getHabits() {return habits;}
    public void setAge(int age) {this.age = age;}
    public int getAge() {return age;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public int getTrickLevel() {return trickLevel;}



    public Pet(String nickname, int age,int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public static void eat() {
        System.out.println("Я кушаю!");
    }
    public abstract void respond();
    @Override
    public abstract String toString();

//    {
//        System.out.println("Создается объект Pet");
//    }
//    static {
//        System.out.println("Загружается класс Pet");
//    }
//
//    protected void finalize() {
//        System.out.println("Удаляется объект: " + this);
//    }
    protected static String getToString(Pet link, String special) {
        String returnedHabits = "[";
        for(String habit : link.getHabits()) {
            returnedHabits += " <" + habit + ">";
        }
        returnedHabits += " ]";
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s, %s}", link.getType(), link.getNickname(), link.getAge(), link.getTrickLevel(), returnedHabits, special);
    }

    public Species getType() {
        return Species.UNKNOWN;
    }

}