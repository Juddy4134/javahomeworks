package com.company.HomeWork2;

import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {
    private static char [][] area = new char[5][5];
    private static boolean finishGame = false;
    private static Scanner SCAN = new Scanner(System.in);
    private static int targetColumn = new Random().nextInt(4);
    private static int targetRow = new Random().nextInt(4);
    private static String row, column;

    public static void main(String[] args) {
        startTheGame();

    }
    private static void startTheGame() {
        System.out.println("All set.Get ready to rumble!");
        fillArea();
        showArea();


        do {
            System.out.printf("Чтобы победить, нужно стрельнуть в %d строку",targetRow);
            System.out.printf(" и в %d колонку \n",targetColumn);

            System.out.println("Enter shooting row");
            row = SCAN.nextLine();

            while (!isValidData(row)){
                System.out.println("Please enter numbers from 1 to 5!");
                row = SCAN.nextLine();
            }

            System.out.println("Enter shooting column");
            column = SCAN.nextLine();

            while (!isValidData(column)){
                System.out.println("Please enter numbers from 1 to 5!");
                column = SCAN.nextLine();
            }

            if (Integer.parseInt(row) - 1 == targetRow & Integer.parseInt(column) - 1 == targetColumn){
                System.out.println("Congratulations, you have won!");
                finishGame = true;
            } else if(area[Integer.parseInt(row) - 1][Integer.parseInt(column) - 1] == '*') {
                System.out.println("You have already shot in that place! Try again!");
                showArea();
            } else if (area[Integer.parseInt(row) - 1][Integer.parseInt(column) - 1] == '-'){
                area[Integer.parseInt(row) - 1][Integer.parseInt(column) - 1] = '*';
                showArea();
            }
        } while (!finishGame);

    }

    private static boolean isValidData(String str) {
        try {
            int x = Integer.parseInt(str);
            return 0 < x && x <= 5;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static void fillArea(){
        for (char[] row : area) {
            for (int i = 0; i < row.length; i++) {
                row[i] = '-';
            }
        }
    }
    private static void showArea () {

        for (int i = 0; i <= area.length; i++) {
            if (i == area.length ) {
                System.out.printf(" %d\n", i);
            } else {
                System.out.printf(" %d |", i);
            }
        }

        for (int j = 0; j < area.length; j++) {
            for (int i = 0; i < area[j].length; i++) {
                if (i == 0) {
                System.out.printf(" %d |",j + 1);
                }
                if (i == area[j].length -1) {
                    System.out.printf(" %c\n", area[j][i]);
                } else {
                    System.out.printf(" %c |", area[j][i]);
                }

            }
        }
    }
}
