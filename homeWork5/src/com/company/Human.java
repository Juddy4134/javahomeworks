package com.company;

import java.util.Arrays;
import java.util.Random;
import static java.util.Objects.isNull;

public class Human {
    private String name, surname;
    private int age, iq;
    private String[][] schedule;
    private Family family;
    private boolean isEmpty = false;

    public void setName(String name) {this.name = name;}
    public String getName() {return name;}
    public void setSurname(String surname) {this.surname = surname;}
    public String getSurname() {return surname;}
    public void setAge(int year) {this.age = age;}
    public int getAge() {return age;}
    public void setIq(int iq) {this.iq = iq;}
    public int getIq() {return iq;}
    public void setSchedule(String[][] schedule) {this.schedule = schedule;}
    public String[][] getSchedule() {return schedule;}
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setFamily() {this.family = null;}

    private void constructor(String name, String surname, int age, int iq) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.iq = iq;
    }

    public Human() {
        isEmpty = true;
    }

    public Human(String name, String surname, int age, int iq) {
        constructor(name, surname, age, iq);
    }

    public Human(String name, String surname, int age, int iq, String[][] schedule) {
        constructor(name, surname, age, iq);
        this.schedule = schedule;
    }


    public void greetPet() {
        System.out.printf("Привет, %s\n", family.getPet().getNickname());
    }
    public void describePet() {
        System.out.printf("У меня есть %s, ему %d лет, он %s\n", family.getPet().getSpecies(), family.getPet().getAge(), family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
    }

    @Override
    public String toString() {
        if(isEmpty) return "Human{}";
        String scheduleToString = "[";
        for(String[] day : schedule) {
            scheduleToString += Arrays.toString(day) + (Arrays.equals(schedule[schedule.length-1], day) ? "]" : ", ");
        }
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, schedule=%s}", name, surname, age, iq, scheduleToString);
    }


    @Override
    public boolean equals(Object person){
        if (person == this){
            return true;
        }
        if (isNull(person) && isNull(this)){
            return true;
        }
        Human guest = (Human) person;
        if (this.getName().equals(guest.getName()) && this.getAge() == guest.getAge()){
            return true;
        } else {
            return false;
        }

    }

    public int hashCode() {
        if (this.isEmpty){
            return 1;
        }
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result +  surname.hashCode();
        result = 31 * result + age;
        result = 31 * result + iq;
        return result;
    }


    public boolean feedPet(boolean isItNeedToFeed) {
        Random random = new Random();
        if(isItNeedToFeed || family.getPet().getTrickLevel() > random.nextInt(101)) {
            System.out.println("Хм... покормлю ка я " + family.getPet().getNickname());
            return true;
        } else {
            System.out.printf("Думаю, %s не голоден.\n", family.getPet().getNickname());
            return false;
        }
    }

    {
        System.out.println("Создается объект Human");
    }
    static {
        System.out.println("Загружается класс Human");
    }

    protected void finalize() {
        System.out.println("Удаляется объект: " + this);
    }

}

