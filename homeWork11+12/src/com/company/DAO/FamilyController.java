package com.company.DAO;

import com.company.Animals.Pet;
import com.company.Family;
import com.company.Human;

import java.util.List;
import java.util.Set;

public class FamilyController  {
    private FamilyService familyService = new FamilyService();

    public CollectionFamilyDao getFamilyDao() {
        return familyService.getFamilyDao();
    }
    public void setFamilies(List<Family> families) {
        this.familyService.setFamilies(families);
    }
    public void setFamilyDao(CollectionFamilyDao familyDao) {
        familyService.setFamilyDao(familyDao);
    }
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public List<Family> displayAllFamilies() {
        return familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int familyMembersLength) {
        return familyService.getFamiliesBiggerThan(familyMembersLength);
    }
    public List<Family> getFamiliesLessThan(int familyMembersLength) {
        return familyService.getFamiliesLessThan(familyMembersLength);
    }
    public int countFamiliesWithMemberNumber(int familyMembersLength) {
        return familyService.countFamiliesWithMemberNumber(familyMembersLength);
    }
    public List<Family> createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }
    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family family, Human child)  {
        return familyService.bornChild(family, child);
    }
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }
    public List<Family> deleteAllChildrenOlderThen(int age) {
        return familyService.deleteAllChildrenOlderThen(age);
    }
    public int count() {
        return familyService.count();
    }
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }
    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }
    public void addPet(Pet pet, int index) {
        familyService.addPet(pet, index);
    }
}
