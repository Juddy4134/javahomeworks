package com.company.homeWork4;

import java.util.Arrays;

public class Pet {
    private String nickname, species;
    private int age, trickLevel;
    private String[] habits;
    private final ConstructorTypes currentType;

    private enum ConstructorTypes {
        SMALL("%s{nickname='%s'}"),
        MAX("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}"),
        EMPTY("pet{}");
        String toStringText;
        ConstructorTypes(String toStringText) {
            this.toStringText = toStringText;
        }
    }

    public void setNickname(String nickname) {this.nickname = nickname;}
    public String getNickname() {return nickname;}
    public void setSpecies(String species) {this.species = species;}
    public String getSpecies() {return species;}
    public void setAge(int age) {this.age = age;}
    public int getAge() {return age;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public int getTrickLevel() {return trickLevel;}
    public void setHabits(String[] habits) {this.habits = habits;}
    public String[] getHabits() {return habits;}

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.currentType = ConstructorTypes.SMALL;
    }

    public Pet(String species, String nickname, int age,int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = Arrays.copyOf(habits, habits.length);

        this.currentType = ConstructorTypes.MAX;
    }

    public Pet() {
        this.currentType = ConstructorTypes.EMPTY;
    }


    public static void eat() {
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.printf("\nПривет, хозяин. Я - %s. Я соскучился!", nickname);
    }
    public static void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return String.format( species, nickname, age, trickLevel, Arrays.toString(habits));
    }

    @Override
    public boolean equals(Object pet){
        if (pet == this){
            return true;
        }
        Pet cutie = (Pet) pet;
        if (this.getNickname().equals(cutie.getNickname()) && this.getAge() == cutie.getAge()){
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int hashCode() {
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + (nickname == null ? 0 : nickname.hashCode());
        result = 31 * result + trickLevel;
        return result;
    }


    {
        System.out.println("Создается объект Pet");
    }
    static {
        System.out.println("Загружается класс Pet");
    }
}
