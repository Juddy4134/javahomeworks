package com.company.DAO;

import com.company.Animals.Pet;
import com.company.Family;
import com.company.Human;

import java.util.List;
import java.util.Set;

public class FamilyController extends FamilyService {
    private FamilyService familyService = new FamilyService();

    @Override
    public CollectionFamilyDao getFamilyDao() {
        return super.getFamilyDao();
    }

    @Override
    public void setFamilyDao(CollectionFamilyDao familyDao) {
        super.setFamilyDao(familyDao);
    }

    @Override
    public List<Family> getAllFamilies() {
        return super.getAllFamilies();
    }

    @Override
    public List<Family> displayAllFamilies() {
        return super.displayAllFamilies();
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int familyMembersLength) {
        return super.getFamiliesBiggerThan(familyMembersLength);
    }

    @Override
    public List<Family> getFamiliesLessThan(int familyMembersLength) {
        return super.getFamiliesLessThan(familyMembersLength);
    }

    @Override
    public int countFamiliesWithMemberNumber(int familyMembersLength) {
        return super.countFamiliesWithMemberNumber(familyMembersLength);
    }

    @Override
    public List<Family> createNewFamily(Human mother, Human father) {
        return super.createNewFamily(mother, father);
    }

    @Override
    public void deleteFamilyByIndex(int index) {
        super.deleteFamilyByIndex(index);
    }

    @Override
    public Family bornChild(Family family, Human child)  {
        return super.bornChild(family, child);
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return super.adoptChild(family, child);
    }

    @Override
    public List<Family> deleteAllChildrenOlderThen(int age) {
        return super.deleteAllChildrenOlderThen(age);
    }

    @Override
    public int count() {
        return super.count();
    }

    @Override
    public Family getFamilyById(int index) {
        return super.getFamilyById(index);
    }

    @Override
    public Set<Pet> getPets(int index) {
        return super.getPets(index);
    }

    @Override
    public void addPet(Pet pet, int index) {
        super.addPet(pet, index);
    }
}
